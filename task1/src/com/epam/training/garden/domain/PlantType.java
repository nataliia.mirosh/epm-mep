//Represents a plant type.

package com.epam.training.garden.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class PlantType {

    private static final PlantType CORN =  new PlantType("Corn", 0.4, 10);
    private static final PlantType PUMPKIN = new PlantType("Pumpkin", 2, 5);
    private static final PlantType GRAPE = new PlantType("Grape", 3, 5);
    private static final PlantType TOMATO = new PlantType("Tomato", 0.3, 10);
    private static final PlantType CUCUMBER = new PlantType("Cucumber", 0.4, 10);

    private String name;
    private double area;
    private double waterAmount;

    public PlantType(String name, double area, double waterAmount) {
        this.name = name;
        this.area = area;
        this.waterAmount = waterAmount;
    }

    public String getName(){
        return name;
    }

    public double getArea() {
        return area;
    }

    public double getWaterAmount() {
        return waterAmount;
    }

    public static List<PlantType> values(){
        return Arrays.asList(CORN, PUMPKIN, GRAPE, TOMATO, CUCUMBER);
    }

    @Override
    public String toString() {
        return "PlantType{" +
                "name='" + name + '\'' +
                ", area=" + area +
                ", waterAmount=" + waterAmount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlantType)) return false;
        PlantType plantType = (PlantType) o;
        return Double.compare(plantType.area, area) == 0 && Double.compare(plantType.waterAmount, waterAmount) == 0 && name.equalsIgnoreCase(plantType.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, area, waterAmount);
    }
}
