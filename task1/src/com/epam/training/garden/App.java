/*
Application main class that creates Garden Service, handles user input and writes result to the
        console.
*/

package com.epam.training.garden;

import com.epam.training.garden.domain.GardenProperties;
import com.epam.training.garden.service.GardenService;
import com.epam.training.garden.service.Result;

import java.util.*;

public class App {

    public static void main(String[] args) {

        var sc = new Scanner(System.in);

        double size;
        double waterSup;
        String plantData;

        try {
            System.out.println("***Welcome to Garden Planner***");
            System.out.println("Please enter your garden properties.");
            System.out.print("Size (square meter): ");
            size = sc.nextDouble();
            sc.nextLine();
            System.out.print("Water supply (in liter): ");
            waterSup = sc.nextDouble();
            sc.nextLine();

            GardenService gardenService = new GardenService();
            GardenProperties gardenProperties = new GardenProperties(size, waterSup);
            gardenService.setGardenProperties(gardenProperties);

            System.out.println(System.lineSeparator() + "Known plant types:");
            for (var suit : gardenService.getPlantTypes()) {
                System.out.println("- " + suit.getName());
            }

            System.out.println(System.lineSeparator() + "Please enter the plants you would like to put in your garden. Press enter when you are done.");
            boolean loop = true;
            Map<String, Integer> addedPlants = new HashMap<>();

            while (loop){
                System.out.print("Enter plant (format: name,amount): ");
                plantData = sc.nextLine();
                if (plantData.equals("")){
                    loop = false;
                } else {
                    addedPlants.put(plantData.substring(0, plantData.indexOf(',')),
                            Integer.valueOf(plantData.substring(plantData.indexOf(',') + 1)));
                }
            }
            System.out.println(System.lineSeparator() + "***Result***" + System.lineSeparator());

            Result result = gardenService.evaluatePlan(addedPlants);
            System.out.format("Required area: %.1f m2%n", result.getArea());
            System.out.format("Water need: %.1f l%n", result.getWaterAmount());
            if ((result.isAreaOk())&&(result.isWaterOk())){
                System.out.println("Plan is feasible in your garden! :)");
            } else {
                System.out.println("Plan is NOT feasible in your garden! :(");
                    if (!result.isAreaOk()){
                        System.out.println("- Not enough area.");
                    }
                    if (!result.isWaterOk()){
                        System.out.println("- Not enough water.");
                    }
            }

        } catch (InputMismatchException e){
            System.out.println("Illegal input format.");
            return;
        } catch (StringIndexOutOfBoundsException e){
            System.out.println("Illegal input format of plants");
            return;
        } catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return;
        }

    }

}
