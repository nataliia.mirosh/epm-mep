/* getPlantTypes() - returns the known plant types in a list.

setGardenProperties() - sets the garden properties.

evaluatePlan() - The application main logic is implemented in this method.
        input: map that has the plant type name as keys, and number of plants as value.
        output: evaluation result.
        Throws IllegalArgumentException if input contains a plan type that is unknown to the application.
*/

package com.epam.training.garden.service;

import com.epam.training.garden.domain.GardenProperties;
import com.epam.training.garden.domain.PlantType;

import java.util.List;
import java.util.Map;

public class GardenService {

    private GardenProperties gardenProperties;

    public GardenService() {
    }

    public List<PlantType> getPlantTypes() {
        return PlantType.values();
    }

    public void setGardenProperties(GardenProperties gardenProperties) {
        this.gardenProperties = gardenProperties;
    }

    public Result evaluatePlan(Map<String, Integer> items) {
        double fullSize = 0;
        double allWaterSupply = 0;

        for (String mapPlantName : items.keySet()) {
            var plantType = getPlantType(mapPlantName);
            fullSize = Double.sum(fullSize, plantType.getArea() * items.get(mapPlantName));
            allWaterSupply = Double.sum(allWaterSupply, plantType.getWaterAmount() * items.get(mapPlantName));
        }
        return new Result(fullSize, allWaterSupply, gardenProperties.getArea() >= fullSize, gardenProperties.getWaterSupply() >= allWaterSupply);

    }

    private PlantType getPlantType(String mapPlantName) {

        List<PlantType> plantTypes = getPlantTypes();

        return plantTypes
                .stream()
                .filter((type) -> mapPlantName.equalsIgnoreCase(type.getName()))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown plant: " + mapPlantName + "."));

    }

}
