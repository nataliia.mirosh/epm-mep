/*      area: the area that all plants requires.

        waterAmount: the amount of water needed by the plants (measured in liter).

        areaOk: true if the area occupied by the plants is smaller than or equals the garden area.

        waterOk: true if the water needed by the plants is smaller than or equals garden watering capacity.
*/

package com.epam.training.garden.service;

import java.util.Objects;

public class Result {

    private double area;
    private double waterAmount;
    private boolean areaOk;
    private boolean waterOk;

    public Result(double area, double waterAmount, boolean areaOk, boolean waterOk) {
        this.area = area;
        this.waterAmount = waterAmount;
        this.areaOk = areaOk;
        this.waterOk = waterOk;
    }

    public double getArea() {
        return area;
    }

    public double getWaterAmount() {
        return waterAmount;
    }

    public boolean isAreaOk() {
        return areaOk;
    }

    public boolean isWaterOk() {
        return waterOk;
    }

    @Override
    public String toString() {
        return "Result{" +
                "area=" + area +
                ", waterAmount=" + waterAmount +
                ", areaOk=" + areaOk +
                ", waterOk=" + waterOk +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Result)) return false;
        Result result = (Result) o;
        return Double.compare(result.area, area) == 0 && Double.compare(result.waterAmount, waterAmount) == 0 && areaOk == result.areaOk && waterOk == result.waterOk;
    }

    @Override
    public int hashCode() {
        return Objects.hash(area, waterAmount, areaOk, waterOk);
    }
}
