package com.epam.training.garden.service;

import com.epam.training.garden.domain.GardenProperties;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class GardenServiceTest {

    @Test
    public void evaluatePlanValidAllTrue() {

        GardenService gardenService = new GardenService();
        gardenService.setGardenProperties(new GardenProperties(14, 125));

        Map<String, Integer> testMap = new HashMap<String, Integer>();
        testMap.put("Corn", 10);
        testMap.put("Pumpkin", 5);

        Assert.assertEquals(new Result(14, 125, true, true), gardenService.evaluatePlan(testMap));

    }

    @Test
    public void evaluatePlanWaterFalse() {

        GardenService gardenService = new GardenService();
        gardenService.setGardenProperties(new GardenProperties(10, 99));

        Map<String, Integer> testMap = new HashMap<String, Integer>();
        testMap.put("Corn", 10);

        Assert.assertEquals(new Result(4, 100, true, false), gardenService.evaluatePlan(testMap));

    }

    @Test
    public void evaluatePlanAllFalse() {

        GardenService gardenService = new GardenService();
        gardenService.setGardenProperties(new GardenProperties(2, 4));

        Map<String, Integer> testMap = new HashMap<String, Integer>();
        testMap.put("Grape", 1);

        Assert.assertEquals(new Result(3, 5, false, false), gardenService.evaluatePlan(testMap));

    }

    @Test
    public void evaluatePlanIllegalArgumentException() {

        GardenService gardenService = new GardenService();

        Map<String, Integer> testMap = new HashMap<String, Integer>();
        testMap.put("Potato", 1);

        IllegalArgumentException thrown = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> gardenService.evaluatePlan(testMap),
                "Unknown plant: Potato."
        );

        Assertions.assertTrue(thrown.getMessage().contains("Unknown plant: Potato."));

    }

}